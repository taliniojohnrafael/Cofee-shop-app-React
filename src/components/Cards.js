import {Row, Col, Card, Button} from 'react-bootstrap'
import React, {useState, useEffect} from 'react';
import Swal from 'sweetalert2';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';

export default function Cards({product}){
    const { _id, name, description, price, quantity, isActive} = product;
    let changeStatus = isActive ?  `Archive` : `Unarchive`;
    let status_product = isActive ? `Active` : `Disabled`;

    const [text, setText] = useState(changeStatus);
    const [text2, setText2] = useState(status_product);

    const [Name, setName] = useState(name);
    const [Description, setDescription] = useState(description);
    const [Price, setPrice] = useState(price);
    const [Quantity, setQuantity] = useState(quantity);

    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

 
    const handleClick = () => {
        fetch(`${process.env.REACT_APP_URI}/products/archive/${_id}`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                Authorization : `Bearer ${localStorage.getItem(`token`)}`
            }
        }).then(response => response.json())
        .then(data =>{
            console.log(data)
            if(!data){
                Swal.fire({
                    title:"Status change failed!",
                    icon: "error",
                })
            
            }
            // console.log(text)
            // console.log(text2)
            if(text == 'Archive'){ 
                setText('Unarchive')
                setText2('Disabled')
            }else{
                setText('Archive')
                setText2('Active');
             }   
            Swal.fire({
                title: "Status changed succes",
                icon: "success",
            })
            
        })
    }

    const clickEdit = (event) => {

        fetch(`${process.env.REACT_APP_URI}/products/update/${_id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization : `Bearer ${localStorage.getItem(`token`)}`
            },
            body: JSON.stringify({
                name: Name,
                description: Description,
                price: Number(Price),
                quantity: Number(Quantity),
            })
        }).then(response => response.json())
        .then(data =>{
            (data !== undefined)
            ?
                Swal.fire({
                    title: "Something went wrong. Please try again!",
                    icon: "error",
                })
            :
            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
            setQuantity(data.quantity);
            console.log(data)

            Swal.fire({
                title: "Status changed succes",
                icon: "success",
            })

        })
    }


	return (
        <Row>
            <div className ="containerPL">
                <div className ="card-bodyPL">
                    <table className="table table-bordered table-hover">
                        <thead className="bg-light text-dark">
                            <tr>
                                    <td className="col-1"><b><i>Status</i></b></td>
                                    <td className="col-2"><b>Name</b></td>
                                    <td className="col-4"><b>Description</b></td>
                                    <td className="col-1"><b>Price</b></td>
                                    <td className="col-1"><b>Stock</b></td>
                                    <td className="col-2"><b>Action</b></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><strong>{text2}</strong></td>

                                <td>{Name}</td>
                                <td>{Description}</td>
                                <td>{Price}</td>
                                <td>{Quantity}</td>
                                {
                                    (changeStatus == 'Unarchive')
                                    ?   <td>
                                            <Button onClick={handleClick}>{text}</Button>
                                            <Button onClick={handleShow} className='px-4 mx-3 bg-success'>Edit</Button>
                                        </td>
                                        
                                    :   <td>
                                            <Button onClick={handleClick}>{text}</Button>
                                            <Button onClick={handleShow} className='px-4 mx-3 bg-success'>Edit</Button>
                                        </td>
                                }
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Edit product</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <Form>
                        <Form.Group as={Row} className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label column sm='2'><b>Name</b></Form.Label>

                            <Col sm='10'>
                                <Form.Control 
                                type="text" 
                                defaultValue={Name}
                                onChange = {event => setName(event.target.value)}
                                />
                            </Col>


                        </Form.Group>

                        <Form.Group as={Row} className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label column sm='2'><b>Price</b></Form.Label>
                            <Col sm='10'>
                                <Form.Control
                                type="text"
                                defaultValue={Price}
                                onChange = {event => setPrice(event.target.value)}
                                />
                            </Col>
                        </Form.Group>


                        <Form.Group as={Row} className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label column sm='2'><b>Stock</b></Form.Label>
                            <Col sm='10'>
                                <Form.Control
                                type="text"
                                defaultValue={Quantity}
                                onChange = {event => setQuantity(event.target.value)}
                                />
                            </Col>
                        </Form.Group>


                        <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                            <Form.Label><b>Description</b></Form.Label>
                            <Form.Control
                            as="textarea" 
                            rows={3}
                            defaultValue={Description}
                            onChange = {event => setDescription(event.target.value)}/>
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>Close</Button>
                    <Button variant="primary" type = "submit" onClick = {function(event){clickEdit(); handleClose()}}>Save Changes</Button>
                </Modal.Footer>
            </Modal>
        {/* onClick={function(){handleClose;handleClick}} */}

        </Row>
        )
}


{/*                 


        (data)
        ?
            if(text === 'Archive'){
                setText('Unarchive');
                setText2('Active');
            }else{
                setText('Archive');
                setText2('Disabled');
            }

            Swal.fire({
                title: "Status changed succes",
                icon: "success",
            })
        :
            Swal.fire({
                title:"Status change failed!",
                icon: "error",
            })
*/}