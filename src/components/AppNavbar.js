/*import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';*/

import {Fragment, useContext} from 'react';

//importing modules using deconstruct
import {Container, Nav, Navbar} from 'react-bootstrap';

import {NavLink} from 'react-router-dom';

import UserContext from '../UserContext';

export default function AppNavbar(){

	//create a state to store the user information
	const {user} = useContext(UserContext);
	//console.log(user)

	return(
		<Navbar expand="lg" className = "vw-100 text-primary py-3 bg-light">
		    <Container fluid>
		        <Navbar.Brand as = {NavLink} to =  "/" className="mx-5"><i>~CoffeeShApp~</i></Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">

		          	<Nav className="ms-auto mx-5">
			            {
							(user.isAdmin)
							?	<Nav.Link as = {NavLink} to = "/">Dashboard</Nav.Link>
							:
								(user.id === null)
								?	<Nav.Link as={NavLink} to="/">Home</Nav.Link>

								:	<><Nav.Link as={NavLink} to="/">Home</Nav.Link><Nav.Link as={NavLink} to="/coffee">My Coffee</Nav.Link></>
						}

			            {	
			            	(user.id !== null)
							?
			            		<Nav.Link as = {NavLink} to = "/logout">Logout</Nav.Link>
							:
			            	<Fragment>

				            	<Nav.Link as = {NavLink} to = "/register">Register</Nav.Link>
				            	<Nav.Link as = {NavLink} to = "/login">Login</Nav.Link>

			            	</Fragment>
			            }


		          	</Nav>
		        </Navbar.Collapse>
		    </Container>
		</Navbar>

	)
}
